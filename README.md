# ChoiceFrame-method-in-Java


import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class ChoiceFrame extends JFrame
{
     // construct text sample
     
     sampleField = new JLabel("Big Java");
     getContentPane().add(sampleField, BorderLayout.CENTER);
     
     // this listener is shared among all components
     class ChoiceListener implements ActionListener
     {
       public void actionPerformed(ActionEvent event)
       {
           setSampleFont();
       }
     }
     
     listener = new ChoiceListener();
     
     createControlPanel();
     setSampleFont();
     pack();
   }
   
   /** 
     Creates the control panel to change the font
   */
   
   public void createControlPanel()
  {
    JPanel facenamePanel = createComboBox();
    JPanel sizeGroupPanel = createCheckBoxes();
    JPanel styleGroupPanel = createRadioButtons();
    
    // line up component panels
    
    JPanel controlPanel = new JPanel();
    controlPanel.setLayout(new GridLayout(3, 1));
    controlPanel.add(facenamePanel);
    controlPanel.add(sizeGroupPanel);
    controlPanel.add(styleGroupPanel);
    
    // add panels to content pane
    
    getContentPane().add(controlPanel, BorderLayout.SOUTH);
  }
  
  public JPanel createComboBox()
  {
      facenameCombo = new JComboBox();
      facenameCombo.addItem("Serif");
      facenameCombo.addItem("Monospaced");
      facenameCombo.addItem(true);
      facenameCombo.addActionListener(listner);
      
      JPanel panel = new JPanel();
      panel.add(facenameCombo);
      return panel;
    }    
    
   /**
     Creates the check boxs for selecting bold and italic style
     @return the panel containing the check boxes
   */
   public JPanel createCheckBoxes();
  {  
      italicCheckBox = new JCheckBox("Italic");
      italicCheckBox.addActionListener(listener);
      
      boldCheckBox = new JCheckBox("Bold");
      boldCheckBox.addActionListener(listener);
      
      JPanel panel = new JPanel();
      panel.add(italicCheckBox);
      panel.add(boldCheckBox);
      panel.setBorder(new TitledBorder(new EtchedBorder(), "Style"));
      
      return panel;
    }
    
    /** 
     Creates the radio buttons to select the font size
     @return the panel containing the radio buttons
    */
    public JPanel createRadioButtons()
    {
      smallButton = new JRadioButton("Small");
      smallButton.addActionListener(listener);
      
      mediumButton = new JRadioButton("Medium");
      mediumButton.addActionListener(listener);
      
      largeButton = new JRadioButton("Large");
      largeButton.addActionListener(listener);
      largeButton.setSelected(true);
      
      // add radio buttons to button group
      
      ButtonGroup group = new ButtonGroup();
      group.add(smallButton);
      group.add(mediumButton);
      group.add(largeButton);
      
      JPanel panel = new JPanel();
      panel.add(smallButton);
      panel.add(mediumButton);
      panel.add(largeButton);
      panel.setBorder(new TitledBorder(new EtchedBorder(), "Size"));
      
      return panel;
    }
    
    public void setSampleFont()
    {
    
     String facename = (String)facenameCombo.getSelectedItem();
     
     // get font style
     
     int style = 0;
     if(italicCheckBox.isSelected())
       style = style + FontITALIC;
      if (boldCheckBox.isSelected())
         style = style + Font.BOLD;
       
       // get font size
       
       int size = 0;
       
       final int SMALL_SIZE = 24;
       final int MEDIUM_SIZE  = 36;
       final int LARGE_SIZE = 48;
       
       if (smallButton.isSelected())
        size = SMALL_SIZE;
       else if (mediumButton.isSelected())
        size = MEDIUM_SIZE;
       else if (largeButton.isSelected())
        size = LARGE_SIZE;
        
        // set font of text field
        
        sampleField.setFont(new Font(facename, style, size));
         sampleField.repaint();
      }
      
      private JLabel sampleField();
      private JCheckBox italicCheckBox;
      private JCheckBox boldCheckBox;
      private JRadioButton smallButton;
      private JRadioButton mediumButton;
      private JRadioButton largeButton;
      private JComboButton facenameCombo;
      private ActionListener listener();
   }   
                   
              
